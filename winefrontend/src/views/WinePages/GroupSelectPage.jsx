import React from "react";
import classnames from "classnames";
import WineContext from "../Context/wine-context"
import { Link } from "react-router-dom";
import winepic from 'assets/img/wine4.jpg';

// reactstrap components
import {
  Card,
  Button,
  Container,
  NavLink,
  Row,
  Col, FormGroup, Label, Input, Form
} from "reactstrap";

// core components
import ExamplesNavbar from "./ExamplesNavbar";

function getCookie(name) {
  if (!document.cookie) {
    return null;
  }
  const token = document.cookie.split(';')
    .map(c => c.trim())
    .filter(c => c.startsWith(name + '='));

  if (token.length === 0) {
    return null;
  }
  return decodeURIComponent(token[0].split('=')[1]);
}

const csrftoken = getCookie('csrftoken')

class GroupSelectPage extends React.Component {
  static contextType = WineContext;
  state = {
    groups: [],
    name: null,
    name1: null,
    member: 4
  };
  componentDidMount() {
    document.body.classList.toggle("group-selection");
    this.getGroups();
  }
  componentWillUnmount() {
    document.body.classList.toggle("group-selection");
  }


  getGroups = () => {
    if(this.context.username == ''){
      alert("Please log in, Thank you!")
      return
    }
    var url = `http://sp19-cs411-46.cs.illinois.edu:8000/api/group/` + this.context.username;
    // var url = `http://127.0.0.1:8000/api/group/` + this.context.username;
    fetch(url,
      {
        method: "GET",
        headers: {
          'X-CSRFToken': csrftoken
        },
      }).then((e) => {
        console.log(e);
        return e.json()
      })
      .catch((e) => { return console.error("Error:", e) })
      .then(e => {
        this.setState({ groups: e.data });
      })
  }


  createGroup = (name, number) => {
    var url = `http://sp19-cs411-46.cs.illinois.edu:8000/api/group/random`;
    var self = this;
    if (number === null) number = 4;
    var username = this.context.username;
    let data = {
      "name": name,
      "number": number,
      "username": username,
    }
    fetch(url,
      {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'X-CSRFToken': csrftoken,
          'Content-Type': 'application/json'
        },
      }).then((e) => {
        console.log(e);
        return e.json()
      })
      .catch((e) => { return console.error("Error:", e) })
      .then(e => {
        console.log(e);
        self.context.updateGroup(e);

      })
  }


  addMember = (gid, member) => {
    // var url = `http://sp19-cs411-46.cs.illinois.edu:8000/api/group/{gid}/members`;
    var url = `http://127.0.0.1:8000/api/group/` + gid + `/members`;
    var formData = new FormData();
    let self = this;
    formData.append("username", this.context.username);
    fetch(url,
      {
        method: "POST",
        body: formData,
        headers: {
          'X-CSRFToken': csrftoken
        },
      }).then((e) => {
        console.log(e);
        return e.json()
      })
      .catch((e) => { return console.error("Error:", e) })
      .then(e => {
        console.log(e);
        self.getGroups();
      })
  }


  testFunction = () => {
    this.createGroup(this.state.name1, this.state.member);
  }


  render() {
    if (this.context.username === '') {
      this.props.history.push('/welcome/register-page')
    }

    var groups = this.state.groups.map(g =>
      <Card style={{ width: "45%", alignItems: "center" }}>
        <img style={{ width: "50%", height: "125px", objectFit: "cover", borderRadius: "50%" }} src={`http://sp19-cs411-46.cs.illinois.edu:8000/static/wid${g.gid}/0.jpg`} />
        <NavLink style={{ fontSize: "large" }} tag={Link} to="group-messages" onClick={() => this.context.updateGroup(g)}>
          {g.name}
        </NavLink>
        <p style={{ width: "80%", wordBreak: "break-word", textAlign: "center" }}>{g.members}</p>
      </Card>
    );

    return (
      <>
        <ExamplesNavbar />

        <div style={{ margin: 20 }} className='section section-basic' >
          <Container>
            <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "space-between" }}>
              {groups}
              <Card style={{ width: "45%", alignItems: "center" }}>
                <FormGroup controlId='wineName'>
                  <Label>Group name:</Label>
                  <Input defaultValue=""
                    placeholder="group"
                    type="text"
                    name='name'
                    value={this.state.name}
                    onChange={e => this.setState({ name1: e.target.value })} />
                </FormGroup>
                <FormGroup controlId='wineMembers'>
                  <Label>Number of members: </Label>
                  <Input defaultValue=""
                    type="number"
                    name='members'
                    value={this.state.members}
                    onChange={e => this.setState({ members: e.target.value })} />
                </FormGroup>
                <Button onClick={this.testFunction}>
                  <NavLink tag={Link} to="group-messages">
                    Meet New Wine Tasters!
                    </NavLink>
                </Button>
              </Card>
            </div>
          </Container>
        </div>

      </>
    );
  }
}

export default GroupSelectPage;
